Source: hipify
Section: devel
Homepage: https://github.com/ROCm/HIPIFY
Priority: optional
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/rocm-team/hipify.git
Vcs-Browser: https://salsa.debian.org/rocm-team/hipify
Maintainer: Debian ROCm Team <debian-ai@lists.debian.org>
Uploaders: Cordell Bloor <cgmb@slerp.xyz>
Build-Depends:
 debhelper-compat (= 13),
 clang-17,
 cmake,
 libclang-17-dev,
 libzstd-dev,
 llvm-17-dev,
 zlib1g-dev,
Rules-Requires-Root: no

Package: hipify-clang
Section: contrib/devel
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
Recommends: nvidia-cuda-toolkit
Description: CUDA to HIP source-to-source translation tool
 hipify-clang is a tool that converts CUDA sources to HIP. It is built on
 libclang and operates on an abstract syntax tree. It can therefore translate
 code containing complex C++ constructs, but it requires input sources to be
 fully compilable.

Package: hipify-perl
Architecture: all
Depends: ${perl:Depends},
         ${misc:Depends},
Description: simple CUDA to HIP source-to-source translation tool
 hipify-perl is a simple tool that processes CUDA sources and converts them to
 HIP sources using textual replacements. It uses a simple perl parser and can
 therefore translate syntactically invalid inputs and source file fragments, but
 it may not recognize constructs that would require more context to correctly
 convert, such as macro expansions or namespace usage.
